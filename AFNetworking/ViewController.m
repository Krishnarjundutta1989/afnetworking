//
//  ViewController.m
//  AFNetworking
//
//  Created by click labs 115 on 11/19/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;

@end

@implementation ViewController
@synthesize imgBack;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getApi];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void) getApi{
    
    
    NSURL *url = [NSURL URLWithString:@"http://www.hdiphone6wallpaper.com/wp-content/uploads/Apple/Apple%20Logo%20iPhone%206%20Wallpapers%20107.jpg"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
      imgBack.image = responseObject;
        //[self saveImage:responseObject withFilename:@"background.png"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
